var hexo = hexo || {};

function title(args){
  var size = args[1] || '';
  var text = '<h1 class="title '+ size +'">' + args[0] + '</h1>';

  return text;
}
hexo.extend.tag.register('title', title);


function subtitle(args){
  var size = args[1] || '';
  var text = '<h2 class="subtitle '+ size +'">' + args[0] + '</h2>';

  return text;
}
hexo.extend.tag.register('subtitle', subtitle);
