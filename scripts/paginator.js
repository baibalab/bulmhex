var hexo = hexo || {};
var pagination = require('hexo-pagination');

function bpaginator(){
  var current = this.page.current;
  var total = this.page.total;
  var base = this.page.base;
  var dir = this.config.pagination_dir;
  var result= '';
  var previous = '<a href="/'+ (base? base:'') + (current-1 === 1 ? '':(dir+'/'+(current-1)) ) + '">Previous</a>';
  var next = '<a href="/'+ (base? base:'') + dir +'/'+ (current+1) + '">Next</a>';


  if(current<total) result +=next;
  if(current>1) result += previous;
  result += '<ul></ul>';

  return result;
}
hexo.extend.helper.register('bpaginator', bpaginator);
