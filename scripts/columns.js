var hexo = hexo || {};

function columns(args, content){
  var messageClass = args[1] || '';
  content = hexo.render.renderSync({text: content, engine: 'markdown'});
  var text = '<div class="columns">' + content + '</div>';

  return text;
}
hexo.extend.tag.register('columns', columns, {ends: true});


function column(args, content){
  var size = args[0] || '';
  content = hexo.render.renderSync({text: content, engine: 'markdown'});
  var text = '<div class="column ' + size + '">' + content + '</div>';

  return text;
}
hexo.extend.tag.register('column', column, {ends: true});
