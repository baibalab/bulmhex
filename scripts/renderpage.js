var hexo = hexo || {};

function renderPage(path){
  var page = hexo.model('Page').findOne({ source: path });

  return page.toObject().content;
}
hexo.extend.helper.register('renderPage', renderPage);
