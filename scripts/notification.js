var hexo = hexo || {};

function notification(args, content){
  var text = '<div class="notification ' + (args[0] || '') + '">' + content + '</div>';
  text = hexo.render.renderSync({text: text, engine: 'markdown'});

  return text;
}
hexo.extend.tag.register('notification', notification, {ends: true});
