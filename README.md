# BulmHex

https://baibalab.gitlab.io/bulmhex/

## Install
1. In the `themes/` directory:
    ```git
    git clone https://gitlab.com/baibalab/bulmhex.git
    ```

2. In the project root directory:
    ```npm
    npm install --save hexo-renderer-bourbon
    npm install --save hexo-renderer-jade
    npm install --save hexo-pagination
    npm install --save bulma
    ```

3. Change the `theme` property in the `config.yml` file.
    ```yml
    # theme: landscape
    theme: bulmhex
    ```

## Documentation

[Documentation](https://baibalab.gitlab.io/bulmhex/docs/)
